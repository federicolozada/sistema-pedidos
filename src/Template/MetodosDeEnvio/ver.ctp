<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MetodosDeEnvio $metodosDeEnvio
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Edit Metodos De Envio'), ['action' => 'editar', $metodosDeEnvio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Metodos De Envio'), ['action' => 'borrar', $metodosDeEnvio->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $metodosDeEnvio->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listado de Metodos De Envio'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Metodos De Envio'), ['action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['controller' => 'Pedidos', 'action' => 'agregar']) ?> </li>
    </ul>
</nav>
<div class="metodosDeEnvio view large-9 medium-8 columns content">
    <h3><?= h($metodosDeEnvio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($metodosDeEnvio->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($metodosDeEnvio->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activo') ?></th>
            <td><?= $metodosDeEnvio->activo ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pedidos') ?></h4>
        <?php if (!empty($metodosDeEnvio->pedidos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nro Pedido') ?></th>
                <th scope="col"><?= __('Cliente Id') ?></th>
                <th scope="col"><?= __('Estado Id') ?></th>
                <th scope="col"><?= __('Envio Id') ?></th>
                <th scope="col"><?= __('Pago Id') ?></th>
                <th scope="col"><?= __('Tienda Id') ?></th>
                <th scope="col"><?= __('Creado') ?></th>
                <th scope="col"><?= __('Actualizado') ?></th>
                <th scope="col"><?= __('Fecha Factura') ?></th>
                <th scope="col"><?= __('Costo Envio') ?></th>
                <th scope="col"><?= __('Intereses') ?></th>
                <th scope="col"><?= __('Total') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            <?php foreach ($metodosDeEnvio->pedidos as $pedidos): ?>
            <tr>
                <td><?= h($pedidos->id) ?></td>
                <td><?= h($pedidos->nro_pedido) ?></td>
                <td><?= h($pedidos->cliente_id) ?></td>
                <td><?= h($pedidos->estado_id) ?></td>
                <td><?= h($pedidos->envio_id) ?></td>
                <td><?= h($pedidos->pago_id) ?></td>
                <td><?= h($pedidos->tienda_id) ?></td>
                <td><?= h($pedidos->creado) ?></td>
                <td><?= h($pedidos->actualizado) ?></td>
                <td><?= h($pedidos->fecha_factura) ?></td>
                <td><?= h($pedidos->costo_envio) ?></td>
                <td><?= h($pedidos->intereses) ?></td>
                <td><?= h($pedidos->total) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Pedidos', 'action' => 'ver', $pedidos->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Pedidos', 'action' => 'editar', $pedidos->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['controller' => 'Pedidos', 'action' => 'borrar', $pedidos->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedidos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
