<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MetodosDeEnvio $metodosDeEnvio
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'borrar', $metodosDeEnvio->id],
                ['confirm' => __('Esta seguro de querer borrar # {0}?', $metodosDeEnvio->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listado de Metodos De Envio'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['controller' => 'Pedidos', 'action' => 'agregar']) ?></li>
    </ul>
</nav>
<div class="metodosDeEnvio form large-9 medium-8 columns content">
    <?= $this->Form->create($metodosDeEnvio) ?>
    <fieldset>
        <legend><?= __('Editar Metodos De Envio') ?></legend>
        <?php
            echo $this->Form->control('nombre');
            echo $this->Form->control('activo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Confirmar')) ?>
    <?= $this->Form->end() ?>
</div>
