<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MetodosDePago $metodosDePago
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Agregar Metodo De Pago'), ['action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="metodosDePago index large-9 medium-8 columns content">
    <h3><?= __('Metodos De Pago') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('activo') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($metodosDePago as $metodosDePago): ?>
            <tr>
                <td><?= $this->Number->format($metodosDePago->id) ?></td>
                <td><?= h($metodosDePago->nombre) ?></td>
                <td><?= h($metodosDePago->activo? 'Si' : 'No') ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'ver', $metodosDePago->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'editar', $metodosDePago->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'borrar', $metodosDePago->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $metodosDePago->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}}')]) ?></p>
    </div>
</div>
