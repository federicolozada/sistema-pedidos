<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MetodosDePago $metodosDePago
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Edit Metodos De Pago'), ['action' => 'editar', $metodosDePago->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Metodos De Pago'), ['action' => 'borrar', $metodosDePago->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $metodosDePago->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listado de Metodos De Pago'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Metodos De Pago'), ['action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['controller' => 'Pedidos', 'action' => 'agregar']) ?> </li>
    </ul>
</nav>
<div class="metodosDePago view large-9 medium-8 columns content">
    <h3><?= h($metodosDePago->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($metodosDePago->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($metodosDePago->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activo') ?></th>
            <td><?= $metodosDePago->activo ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pedidos') ?></h4>
        <?php if (!empty($metodosDePago->pedidos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nro Pedido') ?></th>
                <th scope="col"><?= __('Cliente Id') ?></th>
                <th scope="col"><?= __('Estado Id') ?></th>
                <th scope="col"><?= __('Envio Id') ?></th>
                <th scope="col"><?= __('Pago Id') ?></th>
                <th scope="col"><?= __('Tienda Id') ?></th>
                <th scope="col"><?= __('Creado') ?></th>
                <th scope="col"><?= __('Actualizado') ?></th>
                <th scope="col"><?= __('Fecha Factura') ?></th>
                <th scope="col"><?= __('Costo Envio') ?></th>
                <th scope="col"><?= __('Intereses') ?></th>
                <th scope="col"><?= __('Total') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            <?php foreach ($metodosDePago->pedidos as $pedidos): ?>
            <tr>
                <td><?= h($pedidos->id) ?></td>
                <td><?= h($pedidos->nro_pedido) ?></td>
                <td><?= h($pedidos->cliente_id) ?></td>
                <td><?= h($pedidos->estado_id) ?></td>
                <td><?= h($pedidos->envio_id) ?></td>
                <td><?= h($pedidos->pago_id) ?></td>
                <td><?= h($pedidos->tienda_id) ?></td>
                <td><?= h($pedidos->creado) ?></td>
                <td><?= h($pedidos->actualizado) ?></td>
                <td><?= h($pedidos->fecha_factura) ?></td>
                <td><?= h($pedidos->costo_envio) ?></td>
                <td><?= h($pedidos->intereses) ?></td>
                <td><?= h($pedidos->total) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Pedidos', 'action' => 'ver', $pedidos->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Pedidos', 'action' => 'editar', $pedidos->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['controller' => 'Pedidos', 'action' => 'borrar', $pedidos->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedidos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
