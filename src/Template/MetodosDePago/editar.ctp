<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MetodosDePago $metodosDePago
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'borrar', $metodosDePago->id],
                ['confirm' => __('Esta seguro de querer borrar # {0}?', $metodosDePago->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listado de Metodos De Pago'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['controller' => 'Pedidos', 'action' => 'agregar']) ?></li>
    </ul>
</nav>
<div class="metodosDePago form large-9 medium-8 columns content">
    <?= $this->Form->create($metodosDePago) ?>
    <fieldset>
        <legend><?= __('Editar Metodos De Pago') ?></legend>
        <?php
            echo $this->Form->control('nombre');
            echo $this->Form->control('activo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Confirmar')) ?>
    <?= $this->Form->end() ?>
</div>
