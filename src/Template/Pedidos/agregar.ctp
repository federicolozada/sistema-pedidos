<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pedido $pedido
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Metodos de Envío'), ['controller' => 'MetodosDeEnvio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Metodos De Pago'), ['controller' => 'MetodosDePago', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Tiendas'), ['controller' => 'Tiendas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pedidos form large-9 medium-8 columns content">
    <?= $this->Form->create($pedido) ?>
    <fieldset>
        <legend><?= __('Agregar Pedido') ?></legend>
        <?php
            echo $this->Form->control('nro_pedido');
            echo $this->Form->control('cliente_id', ['options' => $clientes, 'empty' => false]);
            echo $this->Form->control('envio_id', ['options' => $metodosDeEnvio, 'empty' => false]);
            echo $this->Form->control('pago_id', ['options' => $metodosDePago, 'empty' => false]);
            echo $this->Form->control('tienda_id', ['options' => $tiendas, 'empty' => false]);
        ?>
        fecha factura: <input type="date" id="fecha_factura" name="fecha_factura">
    </fieldset>
    <?= $this->Form->button(__('Confirmar')) ?>
    <?= $this->Form->end() ?>
</div>
