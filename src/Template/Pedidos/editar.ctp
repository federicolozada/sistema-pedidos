<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pedido $pedido
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'borrar', $pedido->id],
                ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedido->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Agregar Items al pedido'), ['controller' => 'PedidoItems', 'action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listado de Metodos De Envio'), ['controller' => 'MetodosDeEnvio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Metodos De Envio'), ['controller' => 'MetodosDeEnvio', 'action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Metodos De Pago'), ['controller' => 'MetodosDePago', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Metodos De Pago'), ['controller' => 'MetodosDePago', 'action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Tiendas'), ['controller' => 'Tiendas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Tienda'), ['controller' => 'Tiendas', 'action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Estado'), ['controller' => 'Estados', 'action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Listado de Pedido Items'), ['controller' => 'PedidoItems', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pedidos form large-9 medium-8 columns content">
    <?= $this->Form->create($pedido) ?>
    <fieldset>
        <legend><?= __('Editar Pedido') ?></legend>
        <?php
            echo $this->Form->control('estado_id', ['options' => $estados]);
            echo $this->Form->control('envio_id', ['options' => $metodosDeEnvio, 'empty' => true]);
            echo $this->Form->control('pago_id', ['options' => $metodosDePago, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Confirmar')) ?>
    <?= $this->Form->end() ?>
</div>
