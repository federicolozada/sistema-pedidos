<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pedido $pedido
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Edit Pedido'), ['action' => 'editar', $pedido->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Pedido'), ['action' => 'borrar', $pedido->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedido->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listado de Pedidos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Metodos De Envio'), ['controller' => 'MetodosDeEnvio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Metodos De Envio'), ['controller' => 'MetodosDeEnvio', 'action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Metodos De Pago'), ['controller' => 'MetodosDePago', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Metodos De Pago'), ['controller' => 'MetodosDePago', 'action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Tiendas'), ['controller' => 'Tiendas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Tienda'), ['controller' => 'Tiendas', 'action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Estado'), ['controller' => 'Estados', 'action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Pedido Items'), ['controller' => 'PedidoItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Pedido Item'), ['controller' => 'PedidoItems', 'action' => 'agregar']) ?> </li>
    </ul>
</nav>
<div class="pedidos view large-9 medium-8 columns content">
    <h3><?= h($pedido->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nro Pedido') ?></th>
            <td><?= h($pedido->nro_pedido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cliente') ?></th>
            <td><?= $pedido->cliente->nombre ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= $pedido->estado->estado ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sub-Estado') ?></th>
            <td><?= $pedido->estado->sub_estado ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Metodos De Envio') ?></th>
            <td><?= $pedido->metodos_de_envio->nombre ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Metodos De Pago') ?></th>
            <td><?= $pedido->metodos_de_pago->nombre ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tienda') ?></th>
            <td><?= $pedido->tienda->nombre ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pedido->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Costo Envio') ?></th>
            <td><?= $this->Number->format($pedido->costo_envio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Intereses') ?></th>
            <td><?= $this->Number->format($pedido->intereses) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total') ?></th>
            <td><?= $this->Number->format($pedido->total) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creado') ?></th>
            <td><?= h($pedido->creado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Actualizado') ?></th>
            <td><?= h($pedido->actualizado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Factura') ?></th>
            <td><?= h($pedido->fecha_factura) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pedido Items') ?></h4>
        <?php if (!empty($pedido->pedido_items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Pedido Id') ?></th>
                <th scope="col"><?= __('Item Id') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            <?php foreach ($pedido->pedido_items as $pedidoItems): ?>
            <tr>
                <td><?= h($pedidoItems->id) ?></td>
                <td><?= h($pedidoItems->pedido_id) ?></td>
                <td><?= h($pedidoItems->item_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'PedidoItems', 'action' => 'ver', $pedidoItems->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'PedidoItems', 'action' => 'editar', $pedidoItems->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['controller' => 'PedidoItems', 'action' => 'borrar', $pedidoItems->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedidoItems->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
