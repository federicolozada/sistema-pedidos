<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pedido[]|\Cake\Collection\CollectionInterface $pedidos
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Items'), ['controller' => 'Items','action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Metodos de Envío'), ['controller' => 'MetodosDeEnvio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Metodos De Pago'), ['controller' => 'MetodosDePago', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Tiendas'), ['controller' => 'Tiendas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pedidos index large-9 columns content">
    <h3><?= __('Pedidos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nro_pedido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cliente_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado_id','Sub Estado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('envio_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pago_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tienda_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha_factura') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pedidos as $pedido): ?>
            <tr>
                <td><?= $this->Number->format($pedido->id) ?></td>
                <td><?= h($pedido->nro_pedido) ?></td>
                <td><?= $pedido->cliente->nombre ?></td>
                <td><?= $pedido->estado->estado  ?></td>
                <td><?= $pedido->estado->sub_estado  ?></td>
                <td><?= $pedido->metodos_de_envio->nombre ?></td>
                <td><?= $pedido->metodos_de_pago->nombre ?></td>
                <td><?= $pedido->tienda->nombre ?></td>
                <td><?= h($pedido->fecha_factura->format('d/m/Y')) ?></td>
                <td><?= $this->Number->format($pedido->total) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'ver', $pedido->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'editar', $pedido->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}}')]) ?></p>
    </div>
</div>
