<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Listado de Items'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listado de Pedido Items'), ['controller' => 'PedidoItems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Pedido Item'), ['controller' => 'PedidoItems', 'action' => 'agregar']) ?></li>
    </ul>
</nav>
<div class="items form large-9 medium-8 columns content">
    <?= $this->Form->create($item) ?>
    <fieldset>
        <legend><?= __('Agregar Item') ?></legend>
        <?php
            echo $this->Form->control('nombre');
            echo $this->Form->control('codigo');
            echo $this->Form->control('precio_unitario');
            echo $this->Form->control('cantidad');
            echo $this->Form->control('sub_total');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Confirmar')) ?>
    <?= $this->Form->end() ?>
</div>
