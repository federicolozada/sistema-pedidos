<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Edit Item'), ['action' => 'editar', $item->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar Item'), ['action' => 'borrar', $item->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $item->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listado de Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Item'), ['action' => 'agregar']) ?> </li>
        <li><?= $this->Html->link(__('Listado de Pedido Items'), ['controller' => 'PedidoItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Agregar Pedido Item'), ['controller' => 'PedidoItems', 'action' => 'agregar']) ?> </li>
    </ul>
</nav>
<div class="items view large-9 medium-8 columns content">
    <h3><?= h($item->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($item->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Codigo') ?></th>
            <td><?= h($item->codigo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($item->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Precio Unitario') ?></th>
            <td><?= $this->Number->format($item->precio_unitario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($item->cantidad) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sub Total') ?></th>
            <td><?= $this->Number->format($item->sub_total) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pedido Items') ?></h4>
        <?php if (!empty($item->pedido_items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Pedido Id') ?></th>
                <th scope="col"><?= __('Item Id') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            <?php foreach ($item->pedido_items as $pedidoItems): ?>
            <tr>
                <td><?= h($pedidoItems->id) ?></td>
                <td><?= h($pedidoItems->pedido_id) ?></td>
                <td><?= h($pedidoItems->item_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'PedidoItems', 'action' => 'ver', $pedidoItems->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'PedidoItems', 'action' => 'editar', $pedidoItems->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['controller' => 'PedidoItems', 'action' => 'borrar', $pedidoItems->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedidoItems->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
