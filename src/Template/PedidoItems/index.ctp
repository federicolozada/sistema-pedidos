<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PedidoItem[]|\Cake\Collection\CollectionInterface $pedidoItems
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Lista de Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Lista de Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pedidoItems index large-9 medium-8 columns content">
    <h3><?= __('Pedido Items') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pedido_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('item_id') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pedidoItems as $pedidoItem): ?>
            <tr>
                <td><?= $this->Number->format($pedidoItem->id) ?></td>
                <td><?= $pedidoItem->has('pedido') ? $this->Html->link($pedidoItem->pedido->id, ['controller' => 'Pedidos', 'action' => 'ver', $pedidoItem->pedido->id]) : '' ?></td>
                <td><?= $pedidoItem->has('item') ? $this->Html->link($pedidoItem->item->id, ['controller' => 'Items', 'action' => 'ver', $pedidoItem->item->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'ver', $pedidoItem->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'editar', $pedidoItem->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'borrar', $pedidoItem->id], ['confirm' => __('Esta seguro de querer borrar # {0}?', $pedidoItem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}}')]) ?></p>
    </div>
</div>
