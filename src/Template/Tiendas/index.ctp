<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tienda[]|\Cake\Collection\CollectionInterface $tiendas
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Nueva Tienda'), ['action' => 'agregar']) ?></li>
        <li><?= $this->Html->link(__('Lista de pedidos'), ['controller' => 'Pedidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Agregar Pedido'), ['controller' => 'Pedidos', 'action' => 'agregar']) ?></li>
    </ul>
</nav>
<div class="tiendas index large-9 medium-8 columns content">
    <h3><?= __('Tiendas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="col-1"><?= $this->Paginator->sort('id') ?></th>
                <th class="col-2"><?= $this->Paginator->sort('nombre') ?></th>
                <th class="col-3"><?= $this->Paginator->sort('direccion') ?></th>
                <th class="col-1"><?= $this->Paginator->sort('activo') ?></th>
                <th class="col-3" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tiendas as $tienda): ?>
            <tr>
                <td><?= $this->Number->format($tienda->id) ?></td>
                <td><?= h($tienda->nombre) ?></td>
                <td><?= h($tienda->direccion) ?></td>
                <td><?= h($tienda->activo? 'Si' : 'No') ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'ver', $tienda->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'editar', $tienda->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'borrar', $tienda->id], ['confirm' => __('Esta seguro que desea elmininar la tienda # {0}?', $tienda->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}} total')]) ?></p>
    </div>
</div>

