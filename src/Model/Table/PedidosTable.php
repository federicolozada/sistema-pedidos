<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pedidos Model
 *
 * @property \App\Model\Table\MetodosDeEnvioTable&\Cake\ORM\Association\BelongsTo $MetodosDeEnvio
 * @property \App\Model\Table\MetodosDePagoTable&\Cake\ORM\Association\BelongsTo $MetodosDePago
 * @property \App\Model\Table\TiendasTable&\Cake\ORM\Association\BelongsTo $Tiendas
 * @property \App\Model\Table\PedidoItemsTable&\Cake\ORM\Association\HasMany $PedidoItems
 *
 * @method \App\Model\Entity\Pedido get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pedido newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pedido[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pedido|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pedido saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pedido patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pedido[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pedido findOrCreate($search, callable $callback = null, $options = [])
 * @property \App\Model\Table\EstadosTable&\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\ClientesTable&\Cake\ORM\Association\BelongsTo $Clientes
 */
class PedidosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pedidos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('MetodosDeEnvio')
            ->setForeignKey('envio_id')
            ->setJoinType('INNER');

        $this->belongsTo('MetodosDePago')
            ->setForeignKey('pago_id')
            ->setJoinType('INNER');

        $this->belongsTo('Tiendas')
            ->setForeignKey('tienda_id')
            ->setJoinType('INNER');

        $this->belongsTo('Estados')
            ->setForeignKey('estado_id')
            ->setJoinType('INNER');

        $this->belongsTo('Clientes')
            ->setForeignKey('cliente_id')
            ->setJoinType('INNER');

        $this->hasMany('PedidoItems')
            ->setForeignKey('pedido_id')
            ->setJoinType('INNER');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nro_pedido')
            ->maxLength('nro_pedido', 50)
            ->requirePresence('nro_pedido', 'create')
            ->notEmptyString('nro_pedido');

        $validator
            ->dateTime('creado')
            ->notEmptyDateTime('creado');

        $validator
            ->dateTime('actualizado')
            ->allowEmptyDateTime('actualizado');

        $validator
            ->date('fecha_factura')
            ->allowEmptyDate('fecha_factura');

        $validator
            ->decimal('costo_envio')
            ->allowEmptyString('costo_envio');

        $validator
            ->decimal('intereses')
            ->allowEmptyString('intereses');

        $validator
            ->decimal('total')
            ->allowEmptyString('total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['envio_id'], 'MetodosDeEnvio'));
        $rules->add($rules->existsIn(['pago_id'], 'MetodosDePago'));
        $rules->add($rules->existsIn(['tienda_id'], 'Tiendas'));

        return $rules;
    }
}
