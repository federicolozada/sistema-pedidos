<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MetodosDePago Model
 *
 * @method \App\Model\Entity\MetodosDePago get($primaryKey, $options = [])
 * @method \App\Model\Entity\MetodosDePago newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MetodosDePago[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MetodosDePago|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MetodosDePago saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MetodosDePago patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MetodosDePago[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MetodosDePago findOrCreate($search, callable $callback = null, $options = [])
 * @property \App\Model\Table\PedidosTable&\Cake\ORM\Association\HasMany $Pedidos
 */
class MetodosDePagoTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('metodos_de_pago');
        $this->setDisplayField('nombre');
        $this->setPrimaryKey('id');

        $this->hasMany('Pedidos')
            ->setForeignKey('pago_id')
            ->setJoinType('INNER');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 255)
            ->requirePresence('nombre', 'create')
            ->notEmptyString('nombre');

        $validator
            ->boolean('activo')
            ->notEmptyString('activo');

        return $validator;
    }
}
