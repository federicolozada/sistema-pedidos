<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pedido Entity
 *
 * @property int $id
 * @property string $nro_pedido
 * @property int|null $envio_id
 * @property int|null $pago_id
 * @property int|null $tienda_id
 * @property \Cake\I18n\FrozenTime $creado
 * @property \Cake\I18n\FrozenTime|null $actualizado
 * @property \Cake\I18n\FrozenDate|null $fecha_factura
 * @property float|null $costo_envio
 * @property float|null $intereses
 * @property float|null $total
 *
 * @property \App\Model\Entity\MetodosDeEnvio|null $metodos_de_envio
 * @property \App\Model\Entity\MetodosDePago|null $metodos_de_pago
 * @property \App\Model\Entity\Tienda|null $tienda
 * @property \App\Model\Entity\PedidoItem[] $pedido_items
 * @property int $cliente_id
 * @property int $estado_id
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Cliente $cliente
 */
class Pedido extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nro_pedido' => true,
        'envio_id' => true,
        'pago_id' => true,
        'tienda_id' => true,
        'creado' => true,
        'actualizado' => true,
        'fecha_factura' => true,
        'costo_envio' => true,
        'intereses' => true,
        'total' => true,
        'metodos_de_envio' => true,
        'metodos_de_pago' => true,
        'tienda' => true,
        'pedido_items' => true,
        'cliente_id' => true,
        'estado_id' => true
    ];
}
