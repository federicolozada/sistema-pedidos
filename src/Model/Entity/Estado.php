<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Estado Entity
 *
 * @property int $id
 * @property string $estado
 * @property string $sub_estado
 * @property \App\Model\Entity\Pedido[] $pedidos
 */
class Estado extends Entity
{
    public const CANCELADO_CANCELADO = 1;
    public const COMPLETO_DESPACHADO = 2;
    public const COMPLETO_ENTREGADO = 3;
    public const NUEVO_NECESITO_AYUDA = 4;
    public const NUEVO_PENDIENTE = 5;
    public const PROCESANDO_APROBADO = 6;
    public const PROCESANDO_PREPARANDO_PEDIDO = 7;
    public const RECLAMO_CAMBIO = 8;
    public const RECLAMO_DEVOLUCION = 9;
    public const RECLAMO_PEDIDO = 10;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'estado' => true,
        'sub_estado' => true
    ];
}
