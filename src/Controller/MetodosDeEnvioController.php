<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MetodosDeEnvio Controller
 *
 * @property \App\Model\Table\MetodosDeEnvioTable $MetodosDeEnvio
 *
 * @method \App\Model\Entity\MetodosDeEnvio[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class MetodosDeEnvioController extends CrudController
{

}
