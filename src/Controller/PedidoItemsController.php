<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PedidoItems Controller
 *
 * @property \App\Model\Table\PedidoItemsTable $PedidoItems
 *
 * @method \App\Model\Entity\PedidoItem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class PedidoItemsController extends CrudController
{

}
