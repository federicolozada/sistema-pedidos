<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;
use Cake\Log\Log;
use Crud\Controller\ControllerTrait;

/**
 * Clase base para CRUD / ABM
 *
 * @property \Crud\Controller\Component\CrudComponent $Crud Crud Component
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class CrudController extends AppController
{
    use ControllerTrait;

    /**
     * Configuracion de Paginacion
     * @var array
     */
    public $paginate = [
        'limit' => 50,
    ];

    /**
     * Loads CRUD Plugin
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'index' => 'Crud.Index',
                'ver' => 'Crud.View',
                'agregar' => 'Crud.Add',
                'editar' => 'Crud.Edit',
                'borrar' => 'Crud.Delete',
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
            ]
        ]);
    }

    /**
     * Diferenciar entre Agregar o Editar
     * @param  \Cake\Event\Event  $event Evento
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(Event $event)
    {
        /** @var \Cake\Http\Response $response */
        parent::beforeFilter($event);
        $this->setUpCrudLog();
    }

    /**
     * Activa logs de Crud
     *
     * @return void
     */
    protected function setUpCrudLog()
    {
        $action = $this->request->getParam('action');
        $callback = function (Event $event) use ($action) {
            /** @var \Crud\Event\Subject $subject */
            $subject = $event->getSubject();
            $fallo = false;
            switch ($event->getName()) {
                case 'Crud.afterSave':
                    $fallo = !$subject->success;
                    break;
                case 'Crud.beforeSave':
                    $fallo = $subject->entity->getErrors();
                    break;
            }
            $mensaje = 'Crud ' . $this->name . '::' . $action;
            $mensaje .= ' | Fallo ' . $event->getName();
            if ($fallo) {
                Log::error(
                    $mensaje . PHP_EOL .
                    'post-data: ' . Debugger::exportVar($this->request->getData()) . PHP_EOL .
                    Debugger::exportVar($subject->entity, 10)
                );
            }
        };
        $this->getEventManager()
            ->on('Crud.beforeSave', $callback)
            ->on('Crud.afterSave', $callback);
    }

    /**
     * Wrapper Activar para Acl
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        return $this->Crud->execute();
    }

    /**
     * Wrapper Activar para Acl
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param int $id Entity Id
     * @return \Cake\Http\Response
     */
    public function ver($id = null)
    {
        return $this->Crud->execute();
    }

    /**
     * Wrapper Activar para Acl
     *
     * @return \Cake\Http\Response
     */
    public function agregar()
    {
        return $this->Crud->execute();
    }

    /**
     * Wrapper Activar para Acl
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param int $id Entity Id
     * @return \Cake\Http\Response
     */
    public function editar($id = null)
    {
        return $this->Crud->execute();
    }

    /**
     * Wrapper Activar para Acl
     *
     * @return \Cake\Http\Response
     */
    public function borrar()
    {
        return $this->Crud->execute();
    }
}
