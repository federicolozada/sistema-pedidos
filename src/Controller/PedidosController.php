<?php
namespace App\Controller;

use App\Model\Entity\Estado;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;

/**
 * Pedidos Controller
 *
 * @property \App\Model\Table\PedidosTable $Pedidos
 *
 * @method \App\Model\Entity\Pedido[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ClientesTable $Clientes
 * @property \App\Model\Table\TiendasTable $Tiendas
 * @property \App\Model\Table\MetodosDeEnvioTable $MetodosDeEnvio
 * @property \App\Model\Table\MetodosDePagoTable $MetodosDePago
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class PedidosController extends CrudController
{
    /**
     * Index de Pedidos
     * @return \Cake\Http\Response
     */
    public function index()
    {
        $this->Crud->on('beforePaginate', function (Event $event) {
            $query = $event->getSubject()->query;

            $query->contain([
                'Clientes',
                'Tiendas',
                'MetodosDeEnvio',
                'MetodosDePago',
                'Estados'
            ]);
        });

        return $this->Crud->execute();
    }
    /**
     * Agregar Pedido
     * @return \Cake\Http\Response
     */
    public function agregar()
    {
        $this->Crud->on('beforeRender', function (Event $event) {
            $this->loadModel('Clientes');
            $this->loadModel('Tiendas');
            $this->loadModel('MetodosDeEnvio');
            $this->loadModel('MetodosDePago');
            $clientes = $this->Clientes->find('list')->toArray();
            $tiendas = $this->Tiendas->find('list')->toArray();
            $metodosDeEnvio = $this->MetodosDeEnvio->find('list')->toArray();
            $metodosDePago = $this->MetodosDePago->find('list')->toArray();
            $this->set(compact(
                'clientes',
                'tiendas',
                'metodosDeEnvio',
                'metodosDePago'
            ));
        });

        $this->Crud->on('beforeSave', function (Event $event) {
            $pedido = $event->getSubject()->entity;
            $pedido->estado_id = Estado::NUEVO_PENDIENTE;
        });

        return $this->Crud->execute();
    }
/**
     * Ver Pedido
     * @param integer $id id de la entidad
     * @return \Cake\Http\Response
     */
    public function ver($id = null)
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            $query = $event->getSubject()->query;

            $query->contain([
                'Clientes',
                'Tiendas',
                'MetodosDeEnvio',
                'MetodosDePago',
                'Estados'
            ]);
        });

        return $this->Crud->execute();
    }

    /**
     * Editar Pedido
     * @param integer $id id de la entidad
     * @return \Cake\Http\Response
     */
    public function editar($id = null)
    {
        $this->Crud->on('beforeRender', function (Event $event) {
            $this->loadModel('Estados');
            $this->loadModel('MetodosDeEnvio');
            $this->loadModel('MetodosDePago');
            $metodosDeEnvio = $this->MetodosDeEnvio->find('list')->toArray();
            $metodosDePago = $this->MetodosDePago->find('list')->toArray();
            $estados = $this->Estados->find('list')->toArray();
            $this->set(compact(
                'estados',
                'metodosDeEnvio',
                'metodosDePago'
            ));
        });
        $this->Crud->on('beforeSave', function (Event $event) {
            $pedido = $event->getSubject()->entity;
            $puedeCambiar = $this->puedeCambiar($pedido->getOriginal('estado_id'), $pedido->estado_id);
            if (!$puedeCambiar) {
                $this->Flash->error('No se puede cambiar al estado indicado');
                return $this->redirect($this->referer());
            }
        });

        return $this->Crud->execute();
    }
    /**
     * Funcion para verificar el cambio de estado
     *
     * @param integer $estado_pedido estado del pedido
     * @param integer $estado_a_cambiar id del estado al cual quiere cambiar
     * @return bool
     */
    private function puedeCambiar(int $estado_pedido, int $estado_a_cambiar)
    {
        $permisos = [
            Estado::NUEVO_PENDIENTE => [
                Estado::NUEVO_NECESITO_AYUDA,
                Estado::CANCELADO_CANCELADO,
                Estado::PROCESANDO_APROBADO

            ],
            Estado::CANCELADO_CANCELADO => [
            ],
            Estado::NUEVO_NECESITO_AYUDA => [
                Estado::CANCELADO_CANCELADO,
                Estado::PROCESANDO_APROBADO
            ],
            Estado::PROCESANDO_APROBADO => [
                Estado::RECLAMO_PEDIDO,
                Estado::PROCESANDO_PREPARANDO_PEDIDO
            ],
            Estado::PROCESANDO_PREPARANDO_PEDIDO => [
                Estado::COMPLETO_DESPACHADO,
                Estado::RECLAMO_PEDIDO
            ],
            Estado::COMPLETO_DESPACHADO => [
                Estado::COMPLETO_ENTREGADO,
                Estado::RECLAMO_PEDIDO
            ],
            Estado::COMPLETO_ENTREGADO => [
                Estado::RECLAMO_PEDIDO
            ],
            Estado::RECLAMO_PEDIDO => [
                Estado::RECLAMO_CAMBIO,
                Estado::RECLAMO_DEVOLUCION
            ],
            Estado::RECLAMO_CAMBIO => [
            ],
            Estado::RECLAMO_DEVOLUCION => [
            ],
        ];
        $permitido = $permisos[$estado_pedido];
        return in_array($estado_a_cambiar, $permitido);
    }
}
