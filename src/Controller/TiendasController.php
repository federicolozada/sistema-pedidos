<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tiendas Controller
 *
 * @property \App\Model\Table\TiendasTable $Tiendas
 *
 * @method \App\Model\Entity\Tienda[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class TiendasController extends CrudController
{

}
