<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MetodosDePago Controller
 *
 * @property \App\Model\Table\MetodosDePagoTable $MetodosDePago
 *
 * @method \App\Model\Entity\MetodosDePago[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class MetodosDePagoController extends CrudController
{

}
