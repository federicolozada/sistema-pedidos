<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use Migrations\AbstractMigration;

class ItemsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('items');
        $table
            ->addColumn(
                'nombre', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addColumn(
                'codigo', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addIndex(
                ['codigo'], [
                'name' => 'codigo',
                'unique' => true
                ]
            )
            ->addColumn(
                'precio_unitario', 'decimal', [
                'precision' => 12,
                'scale' => 4,
                'null' => false,
                ]
            )
            ->addColumn(
                'cantidad', 'integer', [
                'limit' => 11,
                'null' => false,
                ]
            )
            ->addColumn(
                'sub_total', 'decimal', [
                'precision' => 12,
                'scale' => 4,
                'null' => false,
                ]
            );

        $table->create();
    }
}
