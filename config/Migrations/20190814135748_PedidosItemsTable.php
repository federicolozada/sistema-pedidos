<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use Migrations\AbstractMigration;

class PedidosItemsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pedido_items');
        $table
            ->addColumn(
                'pedido_id', 'integer', [
                'limit' => 11,
                'null' => false,
                ]
            )
            ->addIndex(
                ['pedido_id'], [
                'name' => 'pedido_id',
                ]
            )
            ->addForeignKey(
                'pedido_id', 'pedidos', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_items_pedido_id',
                ]
            )
            ->addColumn(
                'item_id', 'integer', [
                'limit' => 11,
                'null' => false,
                ]
            )
            ->addIndex(
                ['item_id'], [
                'name' => 'item_id',
                ]
            )
            ->addForeignKey(
                'item_id', 'items', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_items_item_id',
                ]
            );

        $table->create();
    }
}
