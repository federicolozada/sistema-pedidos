<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use Migrations\AbstractMigration;

class TiendasTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tiendas');
        $table
            ->addColumn(
                'nombre', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addColumn(
                'direccion', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addColumn(
                'activo', 'boolean', [
                'default' => 1,
                'null' => false,
                ]
            );

        $table->create();
    }
}
