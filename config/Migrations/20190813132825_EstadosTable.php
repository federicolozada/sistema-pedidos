<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use Migrations\AbstractMigration;

class EstadosTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('estados');
        $table
            ->addColumn(
                'estado', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addColumn(
                'sub_estado', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            );

        $table->create();
        $data = [
            ['estado'=> 'Cancelado', 'sub_estado' => 'Cancelado'],
            ['estado'=> 'Completo', 'sub_estado' => 'Despachado'],
            ['estado'=> 'Completo', 'sub_estado' => 'Entregado'],
            ['estado'=> 'Nuevo', 'sub_estado' => 'Necesito Ayuda'],
            ['estado'=> 'Nuevo', 'sub_estado' => 'Pendiente'],
            ['estado'=> 'Procesando', 'sub_estado' => 'Aprobado'],
            ['estado'=> 'Procesando', 'sub_estado' => 'Preparando Pedido'],
            ['estado'=> 'Reclamo', 'sub_estado' => 'Cambio'],
            ['estado'=> 'Reclamo', 'sub_estado' => 'Devolucion'],
            ['estado'=> 'Reclamo', 'sub_estado' => 'Pedido'],
        ];
        $table->insert($data)->save();
    }
}
