<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use Migrations\AbstractMigration;

class ClientesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('clientes');
        $table
            ->addColumn(
                'nombre', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addColumn(
                'email', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addIndex(
                ['email'], [
                'name' => 'email',
                'unique' => true
                ]
            )
            ->addColumn(
                'telefono', 'string', [
                'limit' => 45,
                'null' => true,
                ]
            )
            ->addColumn(
                'direccion', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            );

        $table->create();
    }
}
