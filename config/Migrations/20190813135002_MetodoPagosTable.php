<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use Migrations\AbstractMigration;

class MetodoPagosTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('metodos_de_pago');
        $table
            ->addColumn(
                'nombre', 'string', [
                'limit' => 255,
                'null' => false,
                ]
            )
            ->addColumn(
                'activo', 'boolean', [
                'default' => 1,
                'null' => false,
                ]
            );

        $table->create();

        $data = [
            ['nombre'=> 'En Efectivo'],
            ['nombre'=> 'Tarjeta de Debito'],
            ['nombre'=> 'Tarjeta de Credito'],
        ];
        $table->insert($data)->save();
    }
}
