<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
use App\Model\Entity\Estado;
use Migrations\AbstractMigration;

class PedidosTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pedidos');
        $table
            ->addColumn(
                'nro_pedido', 'string', [
                'limit' => 50,
                'null' => false,
                ]
            )
            ->addColumn(
                'cliente_id', 'integer', [
                'limit' => 11,
                'null' => false,
                ]
            )
            ->addIndex(
                ['cliente_id'], [
                'name' => 'cliente_id',
                ]
            )
            ->addForeignKey(
                'cliente_id', 'clientes', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_cliente_id',
                ]
            )
            ->addColumn(
                'estado_id', 'integer', [
                'limit' => 11,
                'default' => Estado::NUEVO_PENDIENTE,
                'null' => false,
                ]
            )
            ->addIndex(
                ['estado_id'], [
                'name' => 'estado_id',
                ]
            )
            ->addForeignKey(
                'estado_id', 'estados', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_estado_id',
                ]
            )
            ->addColumn(
                'envio_id', 'integer', [
                'limit' => 11,
                'null' => true,
                ]
            )
            ->addIndex(
                ['envio_id'], [
                'name' => 'envio_id',
                ]
            )
            ->addForeignKey(
                'envio_id', 'metodos_de_envio', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_metodo_envio_id',
                ]
            )
            ->addColumn(
                'pago_id', 'integer', [
                'limit' => 11,
                'null' => true,
                ]
            )
            ->addIndex(
                ['pago_id'], [
                'name' => 'pago_id',
                ]
            )
            ->addForeignKey(
                'pago_id', 'metodos_de_pago', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_metodo_pago_id',
                ]
            )
            ->addColumn(
                'tienda_id', 'integer', [
                'limit' => 11,
                'null' => true,
                ]
            )
            ->addIndex(
                ['tienda_id'], [
                'name' => 'tienda_id',
                ]
            )
            ->addForeignKey(
                'tienda_id', 'tiendas', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'pedido_tienda_id',
                ]
            )
            ->addColumn(
                'creado', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
                'comment' => 'Hora de creacion del pedido',
                ]
            )
            ->addColumn(
                'actualizado', 'timestamp', [
                'null' => true,
                ]
            )
            ->addColumn(
                'fecha_factura', 'date', [
                'null' => true,
                ]
            )
            ->addColumn(
                'costo_envio', 'decimal', [
                'precision' => 12,
                'scale' => 4,
                'null' => true,
                ]
            )
            ->addColumn(
                'intereses', 'decimal', [
                'precision' => 12,
                'scale' => 4,
                'null' => true,
                ]
            )
            ->addColumn(
                'total', 'decimal', [
                'precision' => 12,
                'scale' => 4,
                'null' => true,
                ]
            );

        $table->create();
    }
}
