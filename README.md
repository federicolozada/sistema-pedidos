# sistema-pedidos
ejercicio de sistema de pedidos

## Base de datos

crear en mysql base de datos llamada 'sistema-pedidos'.

Configurar base de datos desde config/app.php utilizando el app.default.php que se encuentra en la misma carpeta.

setear los usuario,contraseña y base de datos en datasourse dentro de app.php

en consola ejecutar bin/cake migrations migrate

(los datos de generacion de tablas se encuentran en config/Migrations)

## Probar en Local

Para probar la app ejecutar ./run.sh

entrar a google chrome o navegador utilizado y en http://localhost:8765/ puede probar la aplicacion.
